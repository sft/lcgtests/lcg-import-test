#!/bin/bash -x
export PYTHONUNBUFFERED=1
mkdir -p $WORKSPACE
cp -r /testpythonimport $WORKSPACE/
ls -la $WORKSPACE/testpythonimport

cd $WORKSPACE/testpythonimport
python python_test.py "$1" "$2"
exit_code=$?
testxml=$WORKSPACE/Test.xml
checksum=$(md5sum ${testxml}|cut -d ' ' -f 1)
curl -s --upload-file ${testxml} "http://lcgapp-cdash.cern.ch/submit.php?project=LCGSoft&FileName=${BUILDHOSTNAME}___${MODE}-$2___${CTEST_TIMESTAMP}-${MODE}___XML___Test.xml&build=${MODE}-$2&site=${BUILDHOSTNAME}&stamp=${CTEST_TIMESTAMP}&MD5=${checksum}"
# DEBUG
#head ${testxml}
#grep ${testxml} -i -e 'date'
#grep ${testxml} -i -e 'time'
# END DEBUG
exit ${exit_code}
