#!/usr/bin/env python
from __future__ import print_function
import importlib
import pkgutil
import sys
import argparse
import os
import traceback

blacklist = ["h5py.tests",  # requires testutil2
             "dateutil.tzwin", "lxml.usedoctest", "lxml.cssselect", "click._winconsole",
             "minrpc.windows", "prompt_toolkit.win32_types", "jpype._windows", "setuptools_scm.win_py31_compat",
             "send2trash.plat_win", 
             "send2trash.plat_win_legacy",
             "send2trash.plat_win_modern",
             "send2trash.plat_osx_ctypes",
             "send2trash.plat_osx_pyobjc",
             "send2trash.IFileOperationProgressSink",
             "psutil._psaix", "psutil._psbsd", "psutil._psosx", "psutil._pssunos",
             "psutil._pswindows", "send2trash.plat_osx",
             "ipykernel._eventloop_macos", "PIL.FpxImagePlugin", "PIL.ImageGrab", "PIL.MicImagePlugin", "PIL.ImageCms", 
             "multiprocess.popen_spawn_win32", "paramiko._winapi", "paramiko.win_pageant", "serial.serialcli", 
             "serial.serialjava", "serial.serialwin32", "serial.win32", "tzlocal.win32", "tzlocal.windows_tz",
             "send2trash.plat_gio",  # requires PyGObject
             "prometheus_client.twisted",  # requires `twisted` - a network library
             "zmq.green",  # requires gevent (greenlet)
             "isort.pylama_isort",  # requires pylama for (optional) code linting
             "IPython.consoleapp", "IPython.kernel",
             "IPython.config", "IPython.frontend", "IPython.html", "IPython.nbconvert",  # deprecated; or add dependency on zeromq (pyzmq) to ipython
             "IPython.nbformat", "IPython.parallel", "IPython.qt",
             "tensorflow.libtensorflow_framework",
             # probably not meant to be imported? "dynamic module does not define init function (initlibtensorflow_framework)"
             "lxml.usedoctest",  # not supposed to be imported directly, used by internal tests
             "storm.testing",  # requires pytest
             "storm.zope",  # requires zope
             "uncertainties.1to2",
             # not supposed to be imported: tool to convert from old (1.x) syntax to new (2.x) one
             "tornado.curl_httpclient",  # requires pycurl
             "sympy.galgebra",
             # FAIL: As of SymPy 1.0 the galgebra module is maintained separately at https://github.com/brombo/galgebra
             "rootpy.root2hdf5",  # requires PyTables
             "py2neo.console",  # depends on neo4j ("graph database")
             "pbr.tests",  # depends on testscenarios package
             "pbr.builddoc", "pbr.sphinxext",  # depends on sphinx
             "qm.setup",  # not supposed to be imported directly
             "pyarrow.cuda", "pyarrow.flight", "pyarrow.orc", "pyarrow.plasma",  # not built by default
             "pyarrow.libarrow_python", # not importable
             "pyarrow.substrait", # not being built
             "dask.array",  # optional, requires: numpy, toolz
             "dask.dataframe",  # optional, requires: locket, toolz, partd, numpy, cloudpickle, pytz, six, python-dateutil, pandas, dask
             "distributed.joblib",  # It is no longer necessary to `import dask_ml.joblib` or `import distributed.joblib`.
             "qtpy.QtCharts", "qtpy.QtWebEngineWidgets",  # optional, not built by default. QtWebEngine is a huge pain to build.
             "distributed._ipython_utils",  # not supposed to be imported directly
             "distributed.asyncio", # deprecated
             "distributed.bokeh", # deprecated
             "partd.numpy", # requires numpy (optional)
             "partd.pandas", # requires pandas (optional)
             "partd.zmq", # requires zmq (optional)
             "pycparser._build_tables",  # not supposed to be imported directly
             "pygments.sphinxext",  # requires docutils
             "pyspark.shell",  # needs additional setup for Java
             "sympy.this",  # print "The Zen of Sympy" - just an easter egg
             "yaml.cyaml",  # cyaml is a wrapper around libyaml and is optional
             "cloudpickle.cloudpickle_fast",  # Requires Python 3.8+
             "simplejson.ordered_dict",  # Not to be imported directly - a replacement for OrderedDict if it's missing (Python <= 2.6)
             "matplotlib.pylab",  # "usage is not recommended"
             "matplotlib.tests", # test data is not installed
             "rpy2.interactive", # see https://bitbucket.org/rpy2/rpy2/issues/542/import-rpy2interactive-as-r-fails-on-rpy2
             "test.autotest",  # crashes
             # FAIL   It is no longer necessary to `import dask_ml.joblib` or `import distributed.joblib`.
             "pyarrow.orc", "pyarrow.plasma",  # not built
             "qtpy.QtCharts", "qtpy.QtWebEngineWidgets",  # underlying Qt modules not built
             "pycparser._build_tables",  # FAIL  [Errno 2] No such file or directory: '_c_ast.cfg'
             "plotly.config",  "plotly.dashboard_objs", "plotly.grid_objs", "plotly.plotly", "plotly.presentation_objs",  
             "plotly.session", "plotly.widgets",  # Deprecated
             "colorcet.plotting", # demo, requires numpy and holoview
             "colorcet.sineramp", # requires numpy
             "jupyterlab.labhubapp",  # requires jupyterhub
             "pandas.conftest",  # FAIL  module 'hypothesis' has no attribute 'unlimited' - maybe update hypothesis?
             "pytools.mpiwrap",  # requires mpi4py
             "pytools.log", # requires logpyle
             "Cython.Coverage",  # code coverage test
             "numpy.conftest",  # requires `pytest`,
             "pathos.mp_map",  # blocks python
             "defusedxml.lxml",  # example code, requires lxml
             "jinja2.asyncfilters",  # inner circular dependency due to how we import the package, harmless https://github.com/pallets/jinja/issues/1144
             "tqdm.keras", # requires a dependency to numpy
             "tqdm.rich",  # requires rich module
             "tqdm.dask",  # requires dask module
             "awkward.libawkward-cpu-kernels",  # not a module
             "awkward.libawkward",  # not a module
             "awkward._v2",  # deprecated module for backward compatibility
             "fsspec.fuse", # requires PyFuse
             "osgeo.utils", # deprecated in favour of osgeo_utils
             "ipykernel.trio_runner", # we don't have trio (opt. dep.)
             "jupyterlab.pytest_plugin",
             "jupyterlab.upgrade_extension", #  Please install cookiecutter
             "requests_unixsocket.testutils", # needs waitress
             "Geant4.g4thread",  # assumed to be obsolete
             "Geant4.g4viscp",  # assumed to be obsolete
             "torch.for_onnx", # ???
             "torchvision._C", # works only on windows
             "torchvision.image", # works only on windows

             # ignore internal torch_sparse modules
             "torch_sparse._convert_cpu",
             "torch_sparse._convert_cuda",
             "torch_sparse._diag_cpu",
             "torch_sparse._diag_cuda",
             "torch_sparse._ego_sample_cpu",
             "torch_sparse._ego_sample_cuda",
             "torch_sparse._hgt_sample_cpu",
             "torch_sparse._hgt_sample_cuda",
             "torch_sparse._metis_cpu",
             "torch_sparse._metis_cuda",
             "torch_sparse._neighbor_sample_cpu",
             "torch_sparse._neighbor_sample_cuda",
             "torch_sparse._relabel_cpu",
             "torch_sparse._relabel_cuda",
             "torch_sparse._rw_cpu",
             "torch_sparse._rw_cuda",
             "torch_sparse._saint_cpu",
             "torch_sparse._saint_cuda",
             "torch_sparse._sample_cpu",
             "torch_sparse._sample_cuda",
             "torch_sparse._spmm_cpu",
             "torch_sparse._spmm_cuda",
             "torch_sparse._spspmm_cpu",
             "torch_sparse._spspmm_cuda",
             "torch_sparse._version_cpu",
             "torch_sparse._version_cuda",

             # ignore internal torch_scatter modules
             "torch_scatter._scatter_cpu",
             "torch_scatter._scatter_cuda",
             "torch_scatter._segment_coo_cpu",
             "torch_scatter._segment_coo_cuda",
             "torch_scatter._segment_csr_cpu",
             "torch_scatter._segment_csr_cuda",
             "torch_scatter._version_cpu",
             "torch_scatter._version_cuda",
             # ignore internal torch_cluster modules
             "torch_cluster._fps_cpu",
             "torch_cluster._graclus_cpu",
             "torch_cluster._grid_cpu",
             "torch_cluster._knn_cpu",
             "torch_cluster._nearest_cpu",
             "torch_cluster._radius_cpu",
             "torch_cluster._rw_cpu",
             "torch_cluster._sampler_cpu",
             "torch_cluster._version_cpu",
             "torch_cluster._fps_cuda",
             "torch_cluster._graclus_cuda",
             "torch_cluster._grid_cuda",
             "torch_cluster._knn_cuda",
             "torch_cluster._nearest_cuda",
             "torch_cluster._radius_cuda",
             "torch_cluster._rw_cuda",
             "torch_cluster._sampler_cuda",
             "torch_cluster._version_cuda",
             
             "s3transfer.crt", # requires awscrt
             'rich._win32_console', # works only on windows
             'rich._windows_renderer', # works only on windows
             'caffe2', # torch.caffe2 is deprecated
             'tvm.micro',  #  not building with MICRO
             'tvm.libtvm',  #  causes internal error, probably double import
             'tvm.libtvm_runtime',  #  causes internal error, probably double import
             'pyshtools.make_docs', # not clear but seems it is used internally
             'dask_kubernetes.conftest',  # needs helm to be installed
             'globus_sdk._testing',  # needs responses, a library for mocking, so test only
             'fire.parser_fuzz_test',  # needs Levenshtein, just for testing
             'googleapiclient._auth',  # needs deprecated httplib2 package
             'googleapiclient.discovery',  # needs deprecated httplib2 package
             'googleapiclient.http',  # needs deprecated httplib2 package
             'googleapiclient.sample_tools',  # needs deprecated httplib2 package
             'apiclient',  # needs deprecated httplib2 package
             'typer._completion_click7',  # needs older click version
             'onnxruntime.transformers',   # needs transformers module, not sure we need it, see SPI-2264
             'verboselogs.pylint',   # needs different (outdated) astroid version
             'tensorboardX.caffe2_graph',  # needs obsolete caffe2 package
             'horovod._keras',  # needs tensorflow package
             'horovod.keras',  # needs tensorflow package
             'horovod.mxnet',  # needs ??? package
             'horovod.ray',     # needs ray package
             'horovod.spark', # needs spark package
             'horovod.tensorflow', # needs torch package
             'horovod.torch', # needs torch package
             'jax.collect_profile', # needs tensorboard_plugin_profile
             'jax_cuda12_plugin.libmosaic_gpu_runtime',  # module cannot be exported
             'mesonbuild._typing', # internal
             'transformers.modeling_flax_outputs', # not installing flax
             'transformers.modeling_flax_pytorch_utils', # not installing flax
             'transformers.modeling_flax_utils', # not installing flax
             'transformers.generation_flax_utils', # not installing flax
             'huggingface_hub._webhooks_payload', #not installing gradio
             'huggingface_hub._webhooks_server', #not installing gradio
             'greenlet.tests',  # needs objgraph, is just a test
             'nltk.book',  # needs some external resource
             'nltk.langnames',  # needs some external resource
             'safetensors.paddle',  # needs paddlepaddle package
             'safetensors.mlx',  # needs mlx package, only for macos
             'func_adl_servicex.local_dataset',  # not installing local dataset
             'kr8s.conftest',  # no installing pytest_kind
             'tests.conftest', # import tests.conftest for zfit
             'pip.__pip-runner__', # cannot run as non-main
             'kfp.deprecated',  # is deprecated
             "webargs.bottleparser",
             "webargs.falconparser",
             "webargs.flaskparser",
             "webargs.pyramidparser",
             "webargs.testing",
             "webargs.djangoparser",
             "aiohttp.worker",
             "httpstan.openapi",
             "couchdb.util2",  # only for python2
             "importlib_resources._py2",
             "gast.ast2",
             "minrpc.file_monitor",
             "mypy.fastparse2", # python2 backward shim
             "hyperopt.main",  # broken file, SerialExperiment module no longer exists
             "langsmith._expect",  # needs langsmith API key
             "pexpect._async_pre_await",  # module for older python versions
             "cv2.cv2",  # something about Submodule name should always start with a parent module name
             "cv2.config-3",  # LOADER_DIR not defined
             "cv2.config",  # LOADER_DIR not defined
             "memray._inject",  # dynamic module cannot be imported
             "pydantic.mypy",  # most likely circular import
             "alembic.testing",
             "optuna.multi_objective", # deprecated module
             "semantic_version.django_fields",  # not adding django
             "faiss.libfaiss_python_callbacks",  # probably this module should not even be there, was not there in 1.8.0
             "confluent_kafka.avro",  # requires avro, not needed by users
             "shortuuid.django_fields",  # requires django, not needed by users [IG 18/02/2025]
             "send2trash.mac",  # mac not used [IG 18/02/2025]
             "send2trash.win",  # win not used [IG 18/02/2025]
             "shellingham.nt",  # win not used [IG 18/02/2025]
             ]

blacklist4 = [
    "zmq.ssh",  # for MacOS
    "tensorboard.data",
    "tensorboard.default",
    "tensorboard.main",
    "tensorboard.program",
    "asn1crypto._elliptic_curve",
    "pyHepMC3.rootIO",
    "psutil._pslinux",
    "cpymad.libmadx",
    "omp",  # main module blacklisted
]

blacklist311 = [
  "astroid._backport_stdlib_names", # only for py <= 3.9
]

blacklist_clang = [
    "xgboost.spark",
]

blacklist_aarch = [
    "xgboost.spark"
]

# add non-default folder for given package
# environment variable (ITK_CORE__HOME) comes from lcgenv_py
RUNTIME_ENV ={'itk': {'sys.path': ['$ITK_CORE__HOME/lib/python%s.%s/site-packages/itk' %
                                   (sys.version_info.major,sys.version_info.minor)]},
              'mpi4py': {'OPAL_PREFIX': [os.environ.get('OPENMPI__HOME','')]},
              'edm4hep': {'ROOT_INCLUDE_PATH': ["$EDM4HEP__HOME/include", "$PODIO__HOME/include"]},
              'podio': {'ROOT_INCLUDE_PATH': ["$PODIO__HOME/include"], 'LD_LIBRARY_PATH': ["$PODIO__HOME/lib64"]},
             }


def test_import(name, quiet):
    f = open("{0}.log".format(name), "w")
    print("Loading main module", name, file=f)
    f.flush()

    if name in blacklist:
      print("Skip loading main module, blacklisted", file=f)
      return True

    # prepare special runtime environment if necessary
    for variable, folders in RUNTIME_ENV.get(name, {}).items():
        pthsep = os.path.pathsep  # e.g ":" on linux
        joinedFolders = os.path.expandvars(pthsep.join(folders))
        if variable == 'sys.path':
            sys.path.append(joinedFolders)
        elif variable in os.environ:
            os.environ[variable] += pthsep + joinedFolders
        else:
            os.environ[variable] = joinedFolders

    try:
        package = importlib.import_module(name)
    except BaseException as e:
        print(name + ": FAIL ", e, file=f)
        print(name + ": FAIL ", e)
        return False

    try:
      prefix = package.__name__ + "."
    except AttributeError:
      # main package does not have __name__, e.g., omp from pythran
      sys.exit(0)
    failed = False
    if not hasattr(package, '__path__'):
        sys.exit(0)

    for importer, modname, ispkg in pkgutil.iter_modules(package.__path__, prefix):
        if not quiet:
            print(name, modname + ": ", end=' ', file=f)
            f.flush()
        if modname.endswith("__main__") or modname in blacklist:
            if not quiet:
                print("SKIP BLACKLIST", file=f)
            continue

        try:
            importlib.import_module(modname)
        except Exception as e:
            if quiet:
                print(name, modname + ": ", end=' ', file=f)

            print("FAIL ", e, file=f)
            print(name, modname + ": FAIL ", e)
            traceback.print_exc()
            failed = True
        else:
            if not quiet:
                print("OK", file=f)

    return not failed


if __name__ == "__main__":
    if sys.platform == 'darwin':
        blacklist.extend(blacklist4)
        os.environ['DYLD_LIBRARY_PATH'] = os.environ['LD_LIBRARY_PATH'] 
    if 'clang' in sys.executable:
        blacklist.extend(blacklist_clang)
    if 'aarch' in sys.executable:
        blacklist.extend(blacklist_aarch)
    if sys.version_info >= (3, 11):
        blacklist.extend(blacklist311)

    parser = argparse.ArgumentParser()
    parser.add_argument("-q", "--quiet", help="Only print failures", action="store_true", dest="quiet")
    parser.add_argument("package")

    args = parser.parse_args()
    if test_import(args.package, args.quiet):
        sys.exit(0)
    else:
        sys.exit(1)


