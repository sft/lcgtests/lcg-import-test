#!/usr/bin/env python
from __future__ import print_function

import argparse
import datetime
import glob
import logging
import os
import time
import platform as _platform
from os import chmod, listdir
from os.path import join, isdir
from subprocess import Popen, TimeoutExpired
from xml.sax.saxutils import escape
import platform
import sys

from xml_templates import xml, xml2, xml3, xmlt

try:
    from colorlog import colorlog
except ImportError:
    colorlog = None

python_version = 'python3'

selected_packages = []

root = ""
rel_root = '/cvmfs/sft.cern.ch/lcg/releases'
dev_root = "/cvmfs/sft-nightlies.cern.ch/lcg/nightlies"

lcgenv = '/cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv_py3'

ignored_packages = ['QMtest', 'pytools', 'pygraphics', 'pyanalysis', 'sip', 
                    'jupyter_latex_envs', 'pjlsa', 
                    'itk_core', 'itk_io', 'itk_filtering', 'itk_numerics',
                    'itk_registration', 'itk_segmentation', 'itk_meshtopolydata',
                    'tensorflow_io_gcs_filesystem',
                    ]

ignored_packages_extra = {'python3': ['hepdata_converter', 'hepdata_validator', 'hspy'],
                          'mac': ['qtpy', 'PyRDF', 'qtconsole', 'pyqt5', 'parsl', 'xgboost'],
                          }

# sip: needs a simpler testing script: from PyQt5 import sip
# TODO: pyspark.shell - java.lang.ClassNotFoundException: org.apache.hadoop.conf.Configuration
# TODO: pjlsa - how to set path to LSA jars?
# > Importing distributed._ipython_utils FAIL:
#   pyzmq/17.1.0/x86_64-centos7-gcc8-opt/lib/python2.7/site-packages/zmq/backend/cython/utils.so:
#   undefined symbol: zmq_curve_public
# TODO: torch & torchvision needs a custom LD_LIBRARY_PATH

ignored_modules = {'future': ['winreg'], 'matplotlib': ['mpl_toolkits'], 'pyxml': ['_xmlplus'], 'jpype': ['jpypex'],
                   'keras': ['docs'], 'sip': ['PyQt5'], 'py2neo': ['test'], 'PyRDF': ['tests'], 'Python': ['__pycache__'],
                   'jinja2': ['asyncfilters'],
                   'PyYAML': ['_yaml'],  # module now deprecated and failing
                  }

ignored_executables = {
  # no help
  'checkdata.py',
  'csv2rdf',
  'epylint',
  'pylint-config',
  'skivi',
  'wait41',
  'workqueue_worker.py',

  # dd4hep executables
  'dumpBfield',
  'dumpdetector',
  'g4FromXML',
  'g4gdmlDisplay',
  'geoConverter',
  'geoDisplay',
  'geoPluginRun',
  'geoWebDisplay',
  'materialBudget',
  'materialScan',
  'print_materials',
  'test_EventReaders',
  'test_cellid_position_converter',
  'test_surface',
  'test_surfaces',
  'test_units',
  'teveDisplay',
  'teveLCIO',
  'dumpBField',
  'graphicalScan',
  # from dd4hep examples
  'CLICSiDAClick',
  'CLICSiDXML',
  'LHeDACLick',
  'LHeDXML',

  # gfal test executables, missing system libraries: libcgsi_plugin.so.1, libglobus_ftp_client.so.2,
  # libglobus_gssapi_gsi.so.4, libglobus_gss_assist.so.3, libgsoap.so.0, libis_ifce.so.1, libvomsapi.so.1
  'gfal_test_checksum',
  'gfal_testchmod',
  'gfal_testcreatdir',
  'gfal_testdir',
  'gfal_testget',
  'gfal_testread',
  'gfal_testrw',
  'gfal_teststat',
  'gfal_testunlink',
  'gfal_unittest',
  'gfal_version',

  # --help && -h returns not 0
  'catior',
  'cconfig',
  'convertior',
  'couchdb-load-design-doc',
  'flatc',
  'gdal_edit.py',
  'gdal_fillnodata.py',
  'gdal_merge.py',
  'gdal_pansharpen.py',
  'gdal_polygonize.py',
  'gdal_proximity.py',
  'gdal_retile.py',
  'gdal_sieve.py',
  'gdalattachpct.py',
  'gdalcompare.py',
  'gdalmove.py',
  'geant4-config',
  'genior',
  'grid.py',
  'mailodf',
  'mpxstats',
  'nameclt',
  'odf2mht',
  'odf2xhtml',
  'odf2xml',
  'odfimgimport',
  'odflint',
  'odfmeta',
  'odfoutline',
  'odfuserfield',
  'ogrmerge.py',
  'omniMapper',
  'omnicpp',
  'pct2rgb.py',
  'pyrcc4',
  'pyrcc5',
  'rgb2pct.py',
  'rst2odt_prepstyles.py',
  'subset.py',
  'svm-predict',
  'svm-scale',
  'svm-train',
  'xml2odf',
  'xrdgsiproxy',
  'xrdsssadmin',

  # we are not building the plasma part of c++ arrow
  'plasma_store',
  # needs flask,
  'parsl-visualize',
  # does not start with hash-bang
  'exec_parsl_function.py',
  # ipython test files
  'iptest', 'iptest3',
  # needs a DISPLAY
  'jupyter-qtconsole',
  # needs oudated version of pyjwt
  'parsl-globus-auth',
  # deprecated, needs waitress and ohers
  'logilab-pytest',
  # exits with 255 after -h
  'onnx_test_runner',
  # tensorflow broken executables in 2.5.0
  'estimator_ckpt_converter',
  'import_pb_to_tensorboard',
  # tensorflow: -h does not exist, (--helpfull returns 1)
  'saved_model_cli',
  # jupyter-labhub missing jupyterhub et al.
  'jupyter-labhub',
  # torch 
  'convert-caffe2-to-onnx',
  'convert-onnx-to-caffe2',

  # kfp
  'kfp',
  'dsl-compile',
  'dsl-compile-v2',
  'dsl-compile-deprecated',

  #gdal, help returns with 1
  'gdal-config',
  'gdal_contour',
  'gdal_create',
  'gdal_grid',
  'gdal_rasterize',
  'gdal_translate',
  'gdal_viewshed',
  'gdaladdo',
  'gdalbuildvrt',
  'gdaldem',
  'gdalenhance',
  'gdalinfo',
  'gdallocationinfo',
  'gdalmanage',
  'gdalmdiminfo',
  'gdalmdimtranslate',
  'gdalsrsinfo',
  'gdaltindex',
  'gdaltransform',
  'gdalwarp',
  'gnmanalyse',
  'gnmmanage',
  'nearblack',
  'ogr2ogr',
  'ogr_layer_algebra.py',
  'ogrinfo',
  'ogrlineref',
  'ogrtindex',
  'sozip',

  '__pycache__',  # dataclasses_json
  'publish.py',  # (dataclasses_json) no help
  'langchain',  # needs docker command to be available
  'langchain-server',  # needs docker command to be available
  'pyftmerge',  # fonttools, help exists with 1
  'tvmc',   # cannot call --help
  'torchfrtrace',  # this binary should not be available, and pulls in some build time only parts of torch
}


# for example to set up specific environment
# package name, list of commands
EXTRA_COMMANDS = {
  'parsl' : [ "export OPAL_PREFIX=${OPENMPI__HOME}" ],
  'podio' : ["export ROOT_INCLUDE_PATH=${PODIO__HOME}/include:$ROOT_INCLUDE_PATH"],
  'pytest' : ["export PYTEST_DISABLE_PLUGIN_AUTOLOAD=1"],
}


failed_executables = set()

# _xmlplus: needs patching, uses reserved keyword `as` as a variable name
# jpypex: should not be imported directly - need to start JVM in advance

runtime_deps = {'keras': ['tensorflow'], 'pygdal': ['XercesC'], 'spark': ['numpy', 'py4j'], 'dask': ['numpy'],
                'rpy2': ['pandas', 'ipython'], 'tensorboard': ['tensorflow'], 'tensorflow_estimator': ['tensorflow'],
                'ipython': ['pyzmq'], 'pyjapc': ['numpy'], 'pywt': ['png'], 'hepdata_converter': ['yoda', 'ROOT'],
                'PyRDF': ['spark', 'ROOT'], 'plotly': ['ipywidgets', 'notebook', 'numpy', 'scipy'], 
                'colorcet': ['scipy'], 'ipydatawidgets': ['ipywidgets', 'numpy', 'scipy'],
                'keras_tuner': ['tensorflow'], 'kt_legacy': ['tensorflow', 'numpy', 'blas'],
                'QKeras': ['tensorflow'], 'tensorflow_model_optimization': ['tensorflow'],
                'matplotlib_inline': ['matplotlib', 'numpy'],
                'dm_tree': ['blas'], 'xarray': ['geos'],
                'tensorboard_plugin_wit': ['tensorflow'],
}

tests = []
logger = None


def setup_logging(logfile, debug, color):
    global logger
    if colorlog is None:
        color = False

    # print("Setup logging (debug is %s)" % (debug,))
    logger = logging.getLogger("import_test")
    logger.propagate = False
    handler = logging.StreamHandler()
    if color:
        handler.setFormatter(
            colorlog.ColoredFormatter(
                '%(asctime)s.%(msecs)03d %(log_color)s[%(name)s:%(levelname)s]%(reset)s %(message)s',
                datefmt='%H:%M:%S'))
    else:
        handler.setFormatter(logging.Formatter(fmt="%(asctime)s.%(msecs)03d [%(name)s:%(levelname)s] %(message)s",
                                               datefmt='%H:%M:%S'))

    file_handler = logging.FileHandler(logfile, "w")
    file_handler.setFormatter(logging.Formatter(fmt="%(asctime)s.%(msecs)03d [%(name)s:%(levelname)s] %(message)s"))

    logger.addHandler(handler)
    logger.addHandler(file_handler)

    if not debug:
        logger.setLevel(logging.INFO)
    else:
        logger.info("Debug logging is ON")
        logger.setLevel(logging.DEBUG)


def execute(cmd):
    p = Popen(cmd, shell=True)
    p.wait()
    return p.returncode == 0


def load_release(platform):
    keys = ('NAME', 'HASH', 'VERSION', 'PATH', 'DEPENDS')
    packages = {}

    fname = join(root, 'LCG_externals_' + platform + '.txt')
    logger.info("Loading release from {0}".format(fname))
    with open(fname) as f:
        for line in f:
            line = [x.strip() for x in line.strip().split(';')]
            if len(line) < 3:
                continue

            if line[0] in ignored_packages or selected_packages and line[0] not in selected_packages:
                logger.debug('Skipping ' + line[0] + '-' + line[2])
                continue
            else:
                logger.debug('Adding ' + line[0] + '-' + line[2])

            while len(line) < len(keys):
                line.append('')

            package = dict(zip(keys, line))
            package['PLATFORM'] = platform
            packages[line[0] + '-' + line[2]] = package

    return packages


def test_package(package, quiet):
    global tests
    retcode = 0
    env_template = 'eval "`{0} -p {1} {{PLATFORM}} {{NAME}} {{VERSION}}`"\n'.format(lcgenv, join(root))

    for pkgname in listdir(package['SITEPACKAGES']):
        if isdir(join(package['SITEPACKAGES'], pkgname)):
            logger.debug("Checking package {0}-{1} version {2} module {3}".format(package['NAME'], 
                                                                                  package['HASH'], 
                                                                                  package['VERSION'], pkgname))

            if pkgname.find('.') != -1:
                logger.debug("{0}: SKIP PKG_INFO".format(pkgname))
                continue
            
            if 'pycache' in pkgname:
                logger.debug("{0}: SKIP PYCACHE".format(pkgname))
                continue

            if pkgname in ignored_modules.get(package['NAME'], []):
                logger.debug("{0}: SKIP BLACKLIST".format(pkgname))
                continue

            this_test = {'name': pkgname, 'path': './testpythonimport/{0}-{1}'.format(package['NAME'], package['VERSION']),
                         'command': [env_template.format(**package).strip()],
                         'log': ''}

            # print("Checking package", p, "module", pkgname)
            with open("test.sh", "w") as fd:
                fd.write("#!/bin/bash \n")
                fd.write(env_template.format(**package))
                for name in runtime_deps.get(package['NAME'], []):
                    dep = {'NAME': name, 'VERSION': '', 'PLATFORM': package['PLATFORM']}
                    this_test['command'].append(env_template.format(**dep).strip())
                    fd.write(env_template.format(**dep))

                this_test['command'].append('python test_import.py {0}'.format(pkgname))
                this_test['command'] = escape(' && '.join(this_test['command']))
                
                fd.write("set -e\n")
                if quiet:
                    fd.write("python -u test_import.py -q {0} 2>{0}.err\n".format(pkgname))
                else:
                    fd.write("python -u test_import.py {0} 2>{0}.err\n".format(pkgname))

            chmod("test.sh", 0o744)
            pd = Popen("./test.sh", shell=False)
            pd.wait()
            failed = pd.returncode != 0
            if failed:
                retcode = 1
                this_test['status'] = 'failed'
                logger.warning(package['NAME'] + "-" + package['VERSION'] + " module " + pkgname + ": FAIL")
            else:
                this_test['status'] = 'passed'
                logger.debug(package['NAME'] + "-" + package['VERSION'] + " module " + pkgname + ": PASS")

            if os.path.exists("{0}.log".format(pkgname)):
                this_test['log'] = escape("--- Standard Output ---\n" + clearLogFile("{0}.log".format(pkgname)))

            if os.path.exists("{0}.err".format(pkgname)):
                this_test['log'] += escape("--- Standard Error ---\n" + clearLogFile("{0}.err".format(pkgname)))

            this_test['log'] = this_test['log'] or 'Unspecified failure'

            if not failed:
                try:
                    os.unlink("{0}.log".format(pkgname))
                except OSError:
                    pass

                try:
                    os.unlink("{0}.err".format(pkgname))
                except OSError:
                    pass
            else:
                os.rename("test.sh", "test_{NAME}-{VERSION}.sh".format(**package))

            tests.append(this_test)

    return retcode

def test_executable(package, executable, quiet):
    global tests
    retcode = 0
    env_template = 'eval "`{0} -p {1} {{PLATFORM}} {{NAME}} {{VERSION}}`"\n'.format(lcgenv, join(root))
    fullPath = os.path.join(package['PATH'], 'bin', executable)

    this_test = {'name': '%s-%s-executable-test' % (package['NAME'], executable),
                 'path': './testpythonexecutable/{0}-{1}'.format(package['NAME'], package['VERSION']),
                 'log': ''}

    # try calling --help/-h/-help, or just the command
    testCommand = '({0} --help || {0} -h || {0} -help || {0})'.format(fullPath)
    with open("test.sh", "w") as fd:
        fd.write("#!/bin/bash \n")
        fd.write(env_template.format(**package))
        for name in runtime_deps.get(package['NAME'], []):
            dep = {'NAME': name, 'VERSION': '', 'PLATFORM': package['PLATFORM']}
            fd.write(env_template.format(**dep))


        fd.write("set -e\n")
        for cmd in EXTRA_COMMANDS.get(package['NAME'], []):
          fd.write(cmd + '\n')
        fd.write("{0} > {1}.log 2> {1}.err\n".format(testCommand,
                                                     executable,
                                                     ))
    this_test['command'] = escape(open("test.sh", "rt").read())

    chmod("test.sh", 0o744)
    try:
        pd = Popen("./test.sh", shell=False)
        pd.wait(timeout=90)  # some commands take a long time to run until help is done
        failed = pd.returncode != 0
    except TimeoutExpired:
        failed = True
        this_test['log'] += 'Test Timed Out!\n'
        pd.kill()

    if failed:
        retcode = 1
        this_test['status'] = 'failed'
        logger.warning(package['NAME'] + "-" + package['VERSION'] + " executable " + executable + ": FAIL")
        failed_executables.add(executable)
    else:
        this_test['status'] = 'passed'
        logger.debug(package['NAME'] + "-" + package['VERSION'] + " executable " + executable + ": PASS")

    if os.path.exists("{0}.log".format(executable)):
        this_test['log'] += escape("--- Standard Output ---\n" + clearLogFile('{0}.log'.format(executable)))

    if os.path.exists("{0}.err".format(executable)):
        this_test['log'] += escape("--- Standard Error ---\n" + clearLogFile('{0}.err'.format(executable)))

    this_test['log'] = this_test['log'] or 'Unspecified failure'

    if not failed:
        for ext in ['log', 'err']:
            try:
                os.unlink("{0}.{1}".format(executable, ext))
            except OSError:
                pass
    else:
        os.rename("test.sh", "test_{NAME}-{VERSION}.sh".format(**package))

    tests.append(this_test)

    return retcode

def clearLogFile(filename):
  """Return content of logfile as string, removes bad characters."""
  fileContent = open(filename, encoding='utf-8').read()
  return "".join(ch for ch in fileContent if ch not in '\b\r\x1b\x1B' and ord(ch)<128)

def main_lcg(args):
    global root
    root = join(rel_root, args.release)
    return test(args)


def main_dev(args):
    global root
    weekday = datetime.date.today().strftime("%a")

    root = join(dev_root, args.release, weekday)
    return test(args)


def main_custom(args):
    global root
    root = args.release
    return test(args)


def test(args):
    retcode = 0
    nExec = 0
    packages = load_release(args.platform)
    logger.debug("Loaded " + str(len(packages)) + " packages")
    for packagename, packageDict in packages.items():
        package_dir = join(args.release, packageDict['PATH'])
        sitepackages_g = join(package_dir, 'lib*/*/site-packages')
        sitepackages = glob.glob(sitepackages_g)

        if len(sitepackages) != 1:
            continue

        sitepackages = sitepackages[0]
        packageDict['SITEPACKAGES'] = sitepackages
        retcode += test_package(packageDict, args.quiet)

        package_dir = join(args.release, packageDict['PATH'])
        binarydir_g = join(package_dir, 'bin/*')
        executables = glob.glob(binarydir_g)

        if not executables:
            continue

        for executable in executables:
            executableName = os.path.basename(executable)
            if executableName in ignored_executables or executableName.endswith(('.sh','.csh')):
                logger.debug("{0}-{1}-executable {2}: SKIP".format(packageDict['NAME'], packageDict['VERSION'], executableName))
                continue
            nExec += 1
            retcode += test_executable(packageDict, executableName, args.quiet)

    logger.info("Tested %d number of executables, %d failed", nExec, retcode)
    logger.info("Failed executables: %s", ' '.join(failed_executables))

    return 1 if retcode > 0 else 0


def main():
    global xml2, selected_packages, python_version, lcgenv

    parser = argparse.ArgumentParser()
    parser.add_argument("-q", "--quiet", help="Only print failures", action="store_true", dest="quiet")
    parser.add_argument("release")
    parser.add_argument("platform")
    parser.add_argument("-p", "--package",
                        help="Only test this list of comma separated packages for failures",
                        action="store",
                        dest="package")

    argz = parser.parse_args()

    if argz.package:
      selected_packages = argz.package.split(',')

    if 'cuda' in argz.release:
        ignored_packages.extend(['tensorflow', 'tensorboard', 'tensorflow_estimator', 'pycuda', 'cupy', 'pyopencl', 'TensorRT', 'root_numpy', 'PyRDF', 'itk', 'itkwidgets'])
    
    if 'nxcals' in argz.release:
        ignored_packages.extend(['hepdata_converter']) #Since we do not build generators for nxcals, hepdata_converter failes to import yoda

    ignored_packages.extend(ignored_packages_extra[python_version])

    if 'mac' in argz.platform:
        ignored_packages.extend(ignored_packages_extra['mac'])

    setup_logging("__python_test.log", not argz.quiet, True)

    if len(argz.release) < 2:
        logger.error("Invaid release:" + str(argz.release))
        exit(1)

    sdate = datetime.datetime.now()

    if argz.release.startswith("LCG_"):
        exit_code = main_lcg(argz)
    elif argz.release[0].isdigit() and argz.release[1].isdigit():
        argz.release = 'LCG_' + argz.release
        exit_code = main_lcg(argz)
    elif argz.release.startswith("dev"):
        exit_code = main_dev(argz)
    elif isdir(argz.release):
        exit_code = main_custom(argz)
    else:
        logger.error("Invaid release:", argz.release)
        exit_code = 1

    edate = datetime.datetime.now()
    delta = (edate - sdate).seconds / 60.
    xmlroot = xml.format(version=argz.release,
                         platform=argz.platform,
                         datetime=os.environ.get('CTEST_TIMESTAMP', sdate.strftime("%Y%m%d-%H%M")),
                         hostname=os.environ.get('BUILDHOSTNAME', _platform.node()),
                         tag=os.environ.get('MODE', argz.release),
                         sdate=sdate.strftime("%b %d %H:%M CEST"),
                         stimestamp=int(time.mktime(sdate.timetuple())))

    testlist = ""

    for this_test in tests:
        testlist += "<Test>{0}/{1}</Test>\n".format(this_test['path'], this_test['name'])
        xml2 += xmlt.format(**this_test)

    xmlroot = xmlroot + testlist.rstrip() + xml2 + xml3.format(edate=edate.strftime("%b %d %H:%M CEST"), etime=delta, etimestamp=int(time.mktime(edate.timetuple())))
    with open(join(os.environ.get('WORKSPACE', os.getcwd()), "Test.xml"), "w") as f:
        f.write(xmlroot)

    exit(exit_code)


if __name__ == '__main__':
    main()
