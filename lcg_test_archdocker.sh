#!/bin/bash -x -e

# Wait until today's nightly is available on stratum 1
if [[ "${LCG_VERSION}" == *"dev"* ]]
then
    today=$( date +%a )
    
    for iterations in {1..12}
    do
        latest_day=$( realpath /cvmfs/sft.cern.ch/lcg/views/${LCG_VERSION}/latest/${PLATFORM}/.. | sed "s|/.*/||g" )
        if [[ "${today}" == "${latest_day}" ]]
        then
            echo "[INFO] Latest nightly is available for testing."
            break
        else
            if  [[ "${iterations}" == "12" ]]
            then
                echo "[ERROR] Abort after two hours of waiting."
                exit 1
            else
                echo "[WARNING] Nightly is not yet present on CVMFS stratum 1. Going to sleep for 10 minutes ..."
                sleep 10m
            fi
        fi
    done
fi

export WORKSPACE_HOST=$WORKSPACE
export WORKSPACE='/workspace'
touch $WORKSPACE_HOST/controlfile

# Docker command
# -w directory                  Working directory inside the container
# -e variable=value             Environment variable to define inside the container 
#    LCG_VERSION=$LCG_VERSION               -> Needed inside cmake/ctest commands
#    LCG_IGNORE=$LCG_IGNORE                 -> Needed inside cmake/ctest commands
#    BUILDMODE=$BUILDMODE                   -> Needed inside cmake/ctest commands
#    LCG_INSTALL_PREFIX=$LCG_INSTALL_PREFIX -> Needed inside cmake/ctest commands
#    LCG_EXTRA_OPTIONS=$LCG_EXTRA_OPTIONS   -> Needed inside cmake/ctest commands  
#    TARGET=$TARGET                         -> Needed inside cmake/ctest commands
#    TEST_LABELS=$TEST_LABELS               -> Needed by tests, it determines kind of tests to run
#    USER=sftnight          -> Needed by some CORAL tests which rely on this env variable 
#    GIT_COMMIT=$GIT_COMMIT -> Needed to avoid repeating update command inside container (otherwise update status appears as failed in cdash)
#    SHELL=$SHELL           -> Needed in some package install instructions using $ENV{SHELL}
# -u username                   User to run all build instructions
# -v host_path:container_path   Folder to bind from docker host to docker container
# gitlab-registry.cern.ch/sft/docker:cc7 image and tag to run in the docker container
# command to execute inside the container (here: script with build instructions followed by params)

case "$LABEL" in
*docker_centos8|*docker_c8*)
        DOCKER_IMAGE=centos8
    ;;
*docker_centos7|*docker_cc7*)
        DOCKER_IMAGE=centos7
    ;;
*docker_slc6*)
        DOCKER_IMAGE=slc6
    ;;
*docker_ubuntu16|*docker_ub16*)
        DOCKER_IMAGE=ubuntu16
    ;;
*docker_ubuntu18|*docker_ub18*)
        DOCKER_IMAGE=ubuntu18
    ;;
*docker_ubuntu20|*docker_ub20*)
        DOCKER_IMAGE=ubuntu20
    ;;
*docker_fedora30|*docker_fc30*)
        DOCKER_IMAGE=fedora30
    ;;
*docker_fedora31|*docker_fc31*)
        DOCKER_IMAGE=fedora31
    ;;
*docker_fedora32|*docker_fc32*)
        DOCKER_IMAGE=fedora32
    ;;
*) echo "Docker image $DOCKER_IMAGE not configured (LABEL: $LABEL)"
   exit 1
   ;;
esac

docker pull gitlab-registry.cern.ch/sft/docker/$DOCKER_IMAGE
docker run -e WORKSPACE="$WORKSPACE" \
            -e USER=sftnight \
            -e SHELL="$SHELL" \
            -e BUILDHOSTNAME="$BUILDHOSTNAME" \
            -e MODE="$MODE" \
            -e CTEST_TAG="$CTEST_TAG" \
            -e CTEST_TIMESTAMP="$CTEST_TIMESTAMP" \
            -u sftnight \
            --name "$NAME" \
            --hostname "$HOSTNAME-docker" \
            --net=host \
            -v /ccache:/ccache \
            -v /ec/conf:/ec/conf \
            -v /cvmfs:/cvmfs \
            -v $PWD/testpythonimport:/testpythonimport \
            gitlab-registry.cern.ch/sft/docker/$DOCKER_IMAGE \
            /bin/bash -x /testpythonimport/runtest-docker.sh "$LCG_VERSION" "$PLATFORM"
