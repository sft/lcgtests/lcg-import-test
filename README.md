# Python Import tests

To run this test yourself you need a machine with cvmfs and the lcg views available and then run from inside this package:

  python3 python_test.py dev3 x86_64-el9-gcc13-opt

(or another view or platform)

To run tests for individual packages use the `--package` flag, e.g.:

  python3 python_test.py dev3 x86_64-el9-gcc13-opt --package QKeras[,Geant4]


## Ignore certain tests

* add to blacklist in test_import.py

### Ignore executables

* add to ignore list in python_test.py
